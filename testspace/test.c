#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <stdbool.h>
#include <unistd.h>

#define handle_error(msg) \
           do { perror(msg); exit(EXIT_FAILURE); } while (0)

#define _MAP_SIZE                           0x1000  // 4096 
#define _PIO_OFFSET                         0xFF200000
#define FALSE                               0
#define TRUE                                1

void test1()
{
    volatile void *gpio_addr;
    volatile unsigned int *gpio_1_addr;
    volatile unsigned int *gpio_2_addr;

    int fd = open("/dev/mem", O_RDWR|O_SYNC);
    if (fd < 0){
        fprintf(stderr, "Unable to open port\n\r");
        exit(fd);
    }


    gpio_addr = mmap(NULL, _MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, _PIO_OFFSET);


    if(gpio_addr == MAP_FAILED){
        handle_error("mmap");
    }
    gpio_1_addr = gpio_addr;
    gpio_2_addr = gpio_addr + 0x4;
    // printf("address: %x value: %x\n", gpio_1_addr, *gpio_1_addr);
    // printf("address: %x value: %x\n", gpio_2_addr, *gpio_2_addr);
    // printf("address: 0xFF200000 value: %x\n", sfr_inl(_PIO_OFFSET));
}

void test2()
{
    char read_buf [256];
    unsigned char msg[] = { 'H', 'e', 'l', 'l', 'o', '\r' };
    int n;
    int fd = open("/dev/tty1", O_RDWR);
    if (fd < 0) \
    {
        printf("Error %i from open: %s\n", errno, strerror(errno));
    }
    else
    {
        printf("open OK\n\r");
    }
    // exit(fd);
    
    n = write(fd, msg, sizeof(msg));
    if(n > 0)
    {
        printf("write OK\n\r");
    }
    else
    {
        printf("Error %i from open: %s\n", errno, strerror(errno));
    }
    n = read(fd, &read_buf, sizeof(read_buf));
    if(n > 0)
    {
        printf("%s\n", read_buf);
    }
    else
    {
        printf("Error %i from open: %s\n", errno, strerror(errno));
        // printf("n = %d\n\r", n);
    }
    
}

void test3()
{
    int fd = open("/dev/ttyS0", O_RDWR|O_SYNC);
    if (fd < 0){
        printf("Unable to open port\n\r");
    }
    exit(fd);
}

int UART0_Open(int fd, char *port)
{
	fd = open(port, O_RDWR|O_NOCTTY|O_NDELAY);
	if(fd==FALSE)
	{
		perror("Can't open the serial port\n");
		return(FALSE);
	}
	if(fcntl(fd, F_SETFL, 0)<0)
	{
		printf("Fcntl failed\n");
		return(FALSE);
	}
	else
	{
		printf("Fcntl=%d\n", fcntl(fd,F_SETFL,0));
	}
	if(isatty(STDIN_FILENO)==0)
	{
		printf("Standard input is not a terminal device\n");
		return(FALSE);
	}
	else
	{
		printf("Isatty success\n");
	}
	printf("fd->open=%d\n", fd);
	return(fd);
}

int UART0_Set(int fd, int speed, int flow_ctrl, int databits, int stopbits, int parity)
{
	int i;
	int status;
	int speed_arr[] = {B1152000, B38400, B19200, B9600, B4800, B2400, B1200, B300};
	int name_arr[] = {115200, 38400, 19200, 9600, 4800, 2400, 1200, 300};
	struct termios options;
	if(tcgetattr(fd, &options)!=0)
	{
		perror("Setup UART\n");
		return(FALSE);
	}

	for( i=0; i < sizeof(speed_arr)/sizeof(int); i++)
	{
		if(speed == name_arr[i])
		{
			cfsetispeed(&options, speed_arr[i]);
			cfsetospeed(&options, speed_arr[i]);
		}
	}     

	options.c_cflag |= CLOCAL;
	options.c_cflag |= CREAD;

	switch(flow_ctrl)
	{
		case 0:	// No flow control is used
			options.c_cflag &= ~CRTSCTS;
			break;
		case 1:	// Hardware flow control
			options.c_cflag |= CRTSCTS;
			break;
		case 2:	// Software flow control
			options.c_cflag |= IXON | IXOFF | IXANY;
			break;
	}

	options.c_cflag &= ~CSIZE;	// Mask other flag bits
	switch (databits)
	{
		case 5:
			options.c_cflag |= CS5;
			break;
		case 6:
			options.c_cflag |= CS6;
			break;
		case 7:
			options.c_cflag |= CS7;
			break;
		case 8:    
			options.c_cflag |= CS8;
			break;  
		default:   
			fprintf(stderr, "Unsupported data size\n");
			return(FALSE);
	}

	switch(parity)
	{  
		case 'n':
		case 'N':	//No Parity bit
			options.c_cflag &= ~PARENB; 
			options.c_iflag &= ~INPCK;    
			break; 
		case 'o':  
		case 'O':	//Odd parity check
			options.c_cflag |= (PARODD | PARENB); 
			options.c_iflag |= INPCK;             
			break; 
		case 'e': 
		case 'E':	//Even parity check
			options.c_cflag |= PARENB;       
			options.c_cflag &= ~PARODD;       
			options.c_iflag |= INPCK;       
			break;
		case 's':
		case 'S':	//Space
			options.c_cflag &= ~PARENB;
			options.c_cflag &= ~CSTOPB;
			break; 
		default:  
			fprintf(stderr, "Unsupported parity\n");   
			return(FALSE); 
	} 

	//Set stop bit
	switch (stopbits)
	{  
		case 1:   
			options.c_cflag &= ~CSTOPB; 
			break; 
		case 2:   
			options.c_cflag |= CSTOPB; 
			break;
		default:   
			fprintf(stderr,"Unsupported stop bits\n"); 
		return (FALSE);
	}

    //Modify output mode, RAW data mode
	options.c_oflag &= ~OPOST;

	//set the minimum waiting time and minimum receiving bytes before unblocking
	options.c_cc[VTIME] = 1;
	options.c_cc[VMIN] = 1;

	tcflush(fd,TCIFLUSH);
	//active the configuration
	if(tcsetattr(fd, TCSANOW, &options) != 0)
	{
		perror("com set error!\n");  
		return (FALSE); 
	}

	return(TRUE);
}

int UART0_Recv(int fd, char *rcv_buf, int data_len)
{
	int len,fs_sel;
	fd_set fs_read;
	struct timeval time;

	FD_ZERO(&fs_read);
	FD_SET(fd, &fs_read);

	time.tv_sec = 10;
	time.tv_usec = 0;

	// Using Select() function to realize the multiply channels' communication
    printf("yee1\t");
    len = read(fd, rcv_buf, data_len);
	fs_sel = select(fd+1, &fs_read, NULL, NULL, &time);
	if(fs_sel)
	{
        printf("yee2\t");
		len = read(fd, rcv_buf, data_len);
		return(len);
	}
	else
	{
		return(FALSE);
	}     
}

int UART0_Send(int fd, char *send_buf, int data_len)
{
	int len = 0;
	len = write(fd, send_buf, data_len);
	if(len == data_len)
	{
		return(len);
	}     
	else
	{
		tcflush(fd,TCOFLUSH);
		return(FALSE);
	}
}

void UART0_Close(int fd)
{
	close(fd);
}

int main()
{
    // unsigned char msg[] = { 'H', 'e', 'l', 'l', 'o', '\r' };
    // unsigned char rcv_buf[256] = {0};
    // int fd;
    // bool set_flg;
    // fd = UART0_Open(fd, "/dev/tty1");
    // set_flg = UART0_Set(fd, 115200, 0, 8, 1, 'o');
    // if(set_flg)
    // {
    //     printf("set succeed!\n");
    // }
    // else
    // {
    //     printf("set failed!\n");
    // }
    // set_flg = UART0_Send(fd, msg, sizeof(msg));
    // if(set_flg)
    // {
    //     printf("send succeed!\n");
    // }
    // else
    // {
    //     printf("send failed!\n");
    // }
    // set_flg = UART0_Recv(fd, &rcv_buf[0] , 256);
    // if(set_flg)
    // {
    //     printf("rcv_buf: %s\n", &rcv_buf[0]);
    // }
    // else
    // {
    //     printf("receive failed!\n");
    // }
    // UART0_Close(fd);
    test2();
    return 0;
}