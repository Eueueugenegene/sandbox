#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <stdbool.h>
#include <unistd.h>

#define handle_error(msg) \
           do { perror(msg); exit(EXIT_FAILURE); } while (0)

#define _MAP_SIZE                           0x1000  // 4096 
#define _MAP_SIZE_2                         0x0100  // 256 
#define _PIO_OFFSET                         0xFF200000
#define _UART0_OFFSET                       0xFFC02000
#define FALSE                               0
#define TRUE                                1

void test1()
{
    volatile void *gpio_addr;
    volatile unsigned int *gpio_1_addr;
    volatile unsigned int *gpio_2_addr;

    int fd = open("/dev/mem", O_RDWR|O_SYNC);
    if (fd < 0){
        fprintf(stderr, "Unable to open port\n\r");
        exit(fd);
    }


    gpio_addr = mmap(NULL, _MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, _PIO_OFFSET);


    if(gpio_addr == MAP_FAILED){
        handle_error("mmap");
    }
    gpio_1_addr = gpio_addr;
    gpio_2_addr = gpio_addr + 0x4;
    // printf("address: %x value: %x\n", gpio_1_addr, *gpio_1_addr);
    // printf("address: %x value: %x\n", gpio_2_addr, *gpio_2_addr);
    // printf("address: 0xFF200000 value: %x\n", sfr_inl(_PIO_OFFSET));
}

void test2()
{
    char read_buf [256];
    unsigned char msg[] = { 'H', 'e', 'l', 'l', 'o', '\r' };
    int n;
    int fd = open("/dev/tty1", O_RDWR);
    if (fd < 0) \
    {
        printf("Error %i from open: %s\n", errno, strerror(errno));
    }
    else
    {
        printf("open OK\n\r");
    }
    // exit(fd);
    
    n = write(fd, msg, sizeof(msg));
    if(n > 0)
    {
        printf("write OK\n\r");
    }
    else
    {
        printf("Error %i from open: %s\n", errno, strerror(errno));
    }
    n = read(fd, &read_buf, sizeof(read_buf));
    if(n > 0)
    {
        printf("%s\n", read_buf);
    }
    else
    {
        printf("Error %i from open: %s\n", errno, strerror(errno));
        // printf("n = %d\n\r", n);
    }
    
}

void test3()
{
    int fd = open("/dev/ttyS0", O_RDWR|O_SYNC);
    if (fd < 0){
        printf("Unable to open port\n\r");
    }
    exit(fd);
}

void test4()
{
    int i;
    volatile void *uart_addr;
    volatile unsigned int *uart_tmp_addr;

    int fd = open("/dev/mem", O_RDWR|O_SYNC);
    if (fd < 0){
        fprintf(stderr, "Unable to open port\n\r");
        exit(fd);
    }


    uart_addr = mmap(NULL, _MAP_SIZE_2, PROT_READ | PROT_WRITE, MAP_SHARED, fd, _UART0_OFFSET);


    if(uart_addr == MAP_FAILED){
        handle_error("mmap");
    }
    uart_tmp_addr = uart_addr+0x8;
    *uart_tmp_addr = 0xC1;
    // printf("address: %x value: %x\n", uart_tmp_addr, *uart_tmp_addr);
    uart_tmp_addr = uart_addr+0xC;
    *uart_tmp_addr = 0x13;
    // printf("address: %x value: %x\n", uart_tmp_addr, *uart_tmp_addr);
    uart_tmp_addr = uart_addr+0x10;
    *uart_tmp_addr = 0xb;
    // printf("address: %x value: %x\n", uart_tmp_addr, *uart_tmp_addr);
    uart_tmp_addr = uart_addr+0x7C;
    *uart_tmp_addr = 0x2;
    // printf("address: %x value: %x\n", uart_tmp_addr, *uart_tmp_addr);
    uart_tmp_addr = uart_addr+0x80;
    *uart_tmp_addr = 0x39;
    // printf("address: %x value: %x\n", uart_tmp_addr, *uart_tmp_addr);
    uart_tmp_addr = uart_addr+0x8C;
    *uart_tmp_addr = 0x1;
    // printf("address: %x value: %x\n", uart_tmp_addr, *uart_tmp_addr);
    uart_tmp_addr = uart_addr+0x98;
    *uart_tmp_addr = 0x1;
    // printf("address: %x value: %x\n", uart_tmp_addr, *uart_tmp_addr);
    uart_tmp_addr = uart_addr+0xF4;
    *uart_tmp_addr = 0x83f32;
    // printf("address: %x value: %x\n", uart_tmp_addr, *uart_tmp_addr);
    uart_tmp_addr = uart_addr+0xF8;
    *uart_tmp_addr = 0x3331312a;
    // printf("address: %x value: %x\n", uart_tmp_addr, *uart_tmp_addr);
    uart_tmp_addr = uart_addr+0xFC;
    *uart_tmp_addr = 0x44570110;
    // printf("address: %x value: %x\n", uart_tmp_addr, *uart_tmp_addr);


    uart_tmp_addr = uart_addr;
    for (i = 0; i < 0x100; i+=0x4)
    {
        printf("address: %x value: %x\n", uart_addr+i, *(uart_tmp_addr+i));
    }
    
    exit(fd);
    // printf("address: %x value: %x\n", gpio_1_addr, *gpio_1_addr);
    // printf("address: %x value: %x\n", gpio_2_addr, *gpio_2_addr); 
}

int main()
{
    test4();
    return 0;
}